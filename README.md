# GitLab Dark Theme

– A somewhat darker theme for many GitLabs

Install at: https://gitlab.com/ntninja/gitlab-dark/raw/master/gitlab-dark.user.css

Based on: https://userstyles.org/styles/160928/gitlab-dark-and-flat

## Contributing

Use the [issues section](https://gitlab.com/ntninja/gitlab-dark/issues) to
browse and create a new issue or contact me at alexander-ws@ninetailed.ninja.

Please also leave a note (or even better: directly open an MR, you can do that
directly on GitLab if you have an account) if you know of other sites that use
GitLab and work with this theme.